# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2019-2020 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "12/05/2020"


from pypushflow.StopActor import StopActor
from pypushflow.addon import BaseActorAddOn, BaseWorkflowAddOn
from pypushflow.AbstractActor import AbstractActor
from ppfmongodb import utils
import logging

_logger = logging.getLogger(__file__)


class StopActorAddOn(BaseActorAddOn):
    """
    Add on to apply on instances of StopActor
    """
    def __init__(self):
        super(StopActorAddOn, self).__init__(target=StopActor)

    def pre_trigger_action(self, actor, in_data):
        if actor.parent is not None and hasattr(actor.parent, 'mongoId'):
            utils.setMongoStatus(actor.parent.mongo_id, 'error')


class AbstractActorAddOn(BaseActorAddOn):
    """
    Add on to apply on instances of BaseActorAddOn
    """
    def __init__(self):
        super(AbstractActorAddOn, self).__init__(target=AbstractActor)

    def uploadDataToMongo(self, actorData={}, script=None):
        if self.parent is not None:
            if self.parent.mongoId is not None:
                if self.actorId is None:
                    actorPath = self.getActorPath() + '/' + self.name
                    self.actorId = utils.initActor(
                        workflowId=self.parent.mongoId,
                        name=actorPath,
                        actorData=actorData,
                        script=script
                    )
                else:
                    utils.addDataToActor(
                        workflowId=self.parent.mongoId,
                        actorId=self.actorId,
                        actorData=actorData
                    )
