#!/usr/bin/env python

from os import path, walk

import sys
from setuptools import setup, find_packages

NAME = "pypushflow mongodb add-on"

VERSION = "0.1.0"

DESCRIPTION = "Publish workflow execution information on a mongodb database"
LONG_DESCRIPTION = open(path.join(path.dirname(__file__), 'README.md')).read()

LICENSE = "MIT"

KEYWORDS = [
    "mongodb", "pypushflow", "add_on", "pymongo"
]

PACKAGES = find_packages()

PACKAGE_DATA = {
}

DATA_FILES = [
]

INSTALL_REQUIRES = [
    'pymongo',
]

ENTRY_POINTS = {

}

NAMESPACE_PACKAGES = ["ppfaddon"]


if __name__ == '__main__':
    setup(
        name=NAME,
        version=VERSION,
        description=DESCRIPTION,
        long_description=LONG_DESCRIPTION,
        license=LICENSE,
        packages=PACKAGES,
        package_data=PACKAGE_DATA,
        data_files=DATA_FILES,
        install_requires=INSTALL_REQUIRES,
        entry_points=ENTRY_POINTS,
        keywords=KEYWORDS,
        namespace_packages=NAMESPACE_PACKAGES,
        include_package_data=True,
        zip_safe=False,
    )
