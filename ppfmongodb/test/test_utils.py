#
# Copyright (c) European Synchrotron Radiation Facility (ESRF)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#


__authors__ = ["O. Svensson", "H. Payno"]
__license__ = "MIT"
__date__ = "28/05/2019"


import os
import logging
import unittest
import pymongo
from ppfmongodb import utils

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger('testPythonActor')


class TestUtilsMongoDb(unittest.TestCase):

    def setUp(self):
        mongo_db_url = 'mongodb://localhost:27017/'
        os.environ['PYPUSHFLOW_MONGOURL'] = mongo_db_url
        os.environ['PYPUSHFLOW_CREATOR'] = 'TestUtilsMongoDb'
        self.mgdbclient = pymongo.MongoClient(mongo_db_url)
        self.data_base_test = self.mgdbclient['TestUtilsMongoDb']

    def tes_initMongo(self):
        name = 'test_initMongo'
        workflowId = utils.initMongo(name=name)
        self.assertIsNotNone(workflowId)

    def tes_initActor(self):
        name = 'test_initMongo'
        workflowId = utils.initMongo(name=name)
        self.assertIsNotNone(workflowId)
        actorName = 'TestActor'
        actorId = utils.initActor(name=actorName, workflowId=workflowId)
        self.assertIsNotNone(actorId)

    def test_addDataToActor(self):
        name = 'test_initMongo'
        print('0')
        workflowId = utils.initMongo(name=name)
        self.assertIsNotNone(workflowId)
        print('a')
        actorName1 = 'TestActor1'
        actorId1 = utils.initActor(name=actorName1, workflowId=workflowId)
        print('b')
        self.assertIsNotNone(actorId1)
        actorName2 = 'TestActor2'
        actorId2 = utils.initActor(name=actorName2, workflowId=workflowId)
        print('c')
        inData = {'a': 1}
        utils.addDataToActor(workflowId=workflowId, actorId=actorId1,
                             actorData={'inData': inData})


def suite():
    test_suite = unittest.TestSuite()
    for ui in (TestUtilsMongoDb, ):
        test_suite.addTest(unittest.defaultTestLoader.loadTestsFromTestCase(ui))
    return test_suite


if __name__ == '__main__':
    unittest.main(defaultTest="suite")
