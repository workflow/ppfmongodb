# coding: utf-8
#/*##########################################################################
# Copyright (C) 2016 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
#############################################################################*/

__authors__ = ["H.Payno",]
__license__ = "MIT"
__copyright__ = "European Synchrotron Radiation Facility, Grenoble, France"
__date__ = "21/02/2018"


import unittest
import logging
from pypushflow.representation import Scheme, Node, Link
from pypushflow.Workflow import ProcessableWorkflow
from pypushflow.addon import utils
from ppfmongodb import settings
import pymongo
import tempfile
import shutil
import os

_logger = logging.getLogger(__name__)


def exec_(scheme, input_=None):
    """
    Simple execution procedure of a workflow.

    :param Scheme scheme:
    :param input_: workflow input if any
    """
    assert isinstance(scheme, ProcessableWorkflow)
    scheme._start_actor.trigger(input_)
    scheme._end_actor.join()
    return scheme._end_actor.out_data


class TestWorkflowAddOn(unittest.TestCase):
    """Test the Workflow add on"""

    def setUp(self):
        self.node1 = Node(processing_pt='ppfhelloworld.test.utils.no_process_function')
        self.mongo_db = pymongo.MongoClient(settings.MONGODB_URL).pybes
        self.collection = self.mongo_db.pybes
        self.collection.drop()
        assert self.collection.find_one() is None
        self.scheme = Scheme(nodes=(self.node1,), links=tuple())

    def tearDown(self):
        self.scheme = None
        self.collection.drop()

    def testHelloWorld(self):
        config = {
            'workflow_name': 'TestWorkflowAddOn',
        }
        processable_workflow = ProcessableWorkflow(scheme=self.scheme,
                                                   configuration=config)
        exec_(scheme=processable_workflow, input_={})
        self.assertTrue(len(self.collection.find_one()) > 0)


class TestActorAddOn(unittest.TestCase):
    def setUp(self):
        self.node1 = Node(
            processing_pt='ppfhelloworld.test.utils.no_process_function')
        self.scheme = Scheme(nodes=(self.node1,), links=tuple())
        self.input_folder = tempfile.mkdtemp()
        self.output_file = os.path.join(self.input_folder, 'input_file.txt')

    def tearDown(self):
        self.scheme = None
        shutil.rmtree(self.input_folder)

    def testHelloWorld(self):
        processable_workflow = ProcessableWorkflow(scheme=self.scheme)
        input = {'output_file': self.output_file}
        exec_(scheme=processable_workflow, input_=input)
        self.assertTrue(os.path.exists(self.output_file))
        with open(self.output_file, 'r') as o_file:
            self.assertEqual(o_file.readlines(), ['one actor processed', ])


class TestAddOnRegistration(unittest.TestCase):
    """Make sure the registration function of pypushflow works"""
    def testRegisteredAddOn(self):
        self.assertTrue('ppfhelloworld' in utils.get_registered_add_ons())

    def testRegisteredClasses(self):
        registred_add_on_c = utils.get_registered_add_ons_classes()
        self.assertTrue('ppfmongodb' in registred_add_on_c)
        import ppfaddon.ppfmongodb.workflowaddon
        self.assertTrue(ppfaddon.ppfmongodb.workflowaddon.WorkflowAddOn in registred_add_on_c['ppfmongodb'])
        import ppfaddon.ppfmongodb.actoraddon
        self.assertTrue(ppfaddon.ppfhelloworld.actoraddon.ActorAddOn in registred_add_on_c['ppfmongodb'])


def suite():
    loader = unittest.defaultTestLoader.loadTestsFromTestCase
    testsuite = unittest.TestSuite()
    testsuite.addTest(loader(TestAddOnRegistration))
    testsuite.addTest(loader(TestWorkflowAddOn))
    testsuite.addTest(loader(TestActorAddOn))
    return testsuite


if __name__ == '__main__':
    runner = unittest.TextTestRunner()
    runner.run(suite())
